# Coding challenge BE

### How to run

```bash
cd src
# For default input.txt and output.txt
python play.py

# For custom input and output files (you need to specify them both)
python play.py input_a.txt output_a.txt

# To test play.py
python test_play.py

# To test ship module
python test_ship.py
```
### File structure

- **src**: Source code
- **data**: Contains the input and output files for play.py

### System dependencies

The code has been implemented and tested in Python 2.7.
Docstring format is following Google Style Python Docstrings.

### Notes

- If a ship tries to move outside the board, it will remain in the box it
currently is.
- If a ship is initialized outside the board it will be ignored.
- A ship cannot move to an occupied cell, even if the ship there is sunk.
- Even if all ships are sunk, the program will continue executing and reading
the input file.
- If an instruction is received for moving a sunk ship, a warning will be printed
to the console and the program will continue its execution
- In case of wrong line in the input file, the program will exit with an error.
- In the second line of input.txt, at least one ship is expected to be initialized correctly.
- In input.txt spacing is allowed between characters, but not any special characters
or quotes.
