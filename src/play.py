#!/usr/bin/env python
"""Module for board initialization, ship movement and outputting state"""

import sys
import ast
import re
import os
from os.path import join as pjoin
from ship import Ship, ROOT


def fprint(message):
    print('*'*20)
    print(message)
    print('*'*20)

def catch_error_wrapper(func, line, error_message):
    """A wrapper for executing functions and exiting in case of failure.

    Args:
        func (function): The function to evaluate.
        line (str): The argument to pass to the function.
        error_message (str): The error message to show in case of exception.

    Returns:
        res: The output of func(line).

    """
    try:
        res = func(line)
    except:
        print('Error: ' + error_message)
        sys.exit(1)
    return res

def initialize_ships(line):
    """A function for parsing for the second line of input.txt and initializing ships.

    Args:
        line (str): A string with ship coordinates in the form (x, y, O).

    Returns:
        ships (dict): A dictionary with ship coordinates for keys ('x|y') and
                      Ship instances for values.

    """
    ships = {}

    # Convert line to array of tuples
    line = line.strip().replace(" ", "").replace(")", "),")
    line = re.sub(r'([NEWS])', r'"\1"', line)
    str_coordinates = ast.literal_eval(line)

    # If array is empty terminate execution
    if len(str_coordinates) == 0:
        raise ValueError('You should define at least one ship correctly')

    # For every tuple (x, y, O) create a Ship instance
    for i, crd in enumerate(str_coordinates):
        x, y, orientation = crd
        ships['{}|{}'.format(x, y)] = Ship(x, y, orientation, i)

    return ships

def ships_on_board(ships, N):
    """A function for keeping in the ships dictionary only the ships on board.

    Args:
        ships (dict): A dictionary with ship coordinates for keys ('x|y') and
                      Ship instances for values.
        N (int): The board size

    Returns:
        ships (dict): A dictionary with ship coordinates for keys ('x|y') and
                      Ship instances for values.

    """
    # Remove any ships that are not on board
    for xy in ships.keys():
        x, y = xy.split('|')
        if not (int(x) in range(0, N) and int(y) in range(0, N)):
            ships.pop(xy, None)

    # Verify that there are ships on board
    if len(ships.keys()) == 0:
        raise ValueError('You should define at least one ship correctly')
    return ships

def parse_movement(line):
    """A parser for ship movement or shooting.

    Args:
        line (str): A string in the format '(x, y) LLL' for movement or '(x, y)'
                    for shooting.

    Returns:
        (tuple): A tuple with coordinates and navigation commands or coordinates
                 only.

    """
    line = line.strip().replace(" ", "").replace(")", ") ")
    coord, movement = line.split(' ')
    coord = ast.literal_eval(coord)

    def valid_coord(coord):
        """Verify coord is (int,int)"""
        return isinstance(coord[0], int) and isinstance(coord[1], int) and len(coord)==2

    def valid_move(strg, search=re.compile(r'[^(M|L|R)]').search):
        """Validate movement is space or capital characters"""
        return not bool(search(strg))

    if not valid_coord(coord) or not valid_move(movement):
        raise ValueError('Wrong movement command')
    if len(movement) > 0:
        return (coord, movement)
    return (coord,)

def write_output(output_name, ships):
    """A function for writing the board state to an output file.

    Ships are written to the output file in the same order their coordinates
    were read.

    Args:
        output_name (str): The name of the file to write the board state.
        ships (dict): A dictionary with ship coordinates for keys ('x|y') and
                      Ship instances for values.

    """
    # sort ships by index
    def dict_val(x):
        return x[1].index
    sorted_ships = sorted(ships.items(), key=dict_val)

    # write state to the output file
    output_path = pjoin(ROOT, 'data', output_name)
    output_f = open(output_path, "w")
    for key, ship in sorted_ships:
        output_f.write(ship.output_state() + "\n")
    output_f.close()

def play(input_txt, output_txt):
    """The main function that runs the end to end system.

    Initial state of board is read, series of operations is applied and final
    state is written to an output file.

    Args:
        input_txt (str): The name of the file to read the initial board state and
                         a series of operations.
        output_txt (str): The name of the file to write the final board state.

    """
    # Open input file
    input_path = pjoin(ROOT, 'data', input_txt)
    input_f = open(input_path, 'r')

    # Read file line by line
    for i, line in enumerate(input_f):
        fprint("Reading line: {}".format(line.strip()))
        if i == 0:
            N = catch_error_wrapper(int, line, 'First line should be board size')
            print('Board size is {}'.format(N))
        elif i == 1:
            # check that when initializing the coordinates are inside the board
            ships = catch_error_wrapper(initialize_ships, line, 'Incorrect ship coordinates')
            # keep only the ships whose coordinates are on the board
            ships = ships_on_board(ships, N)
        else:
            movement_cmd = catch_error_wrapper(parse_movement, line, 'Incorrect ship movement')
            x, y = movement_cmd[0]
            xy = '{}|{}'.format(x, y)

            if len(movement_cmd) == 1:
                # shoot
                if ships.has_key(xy):
                    print('Shoot at cell: {}'.format(xy))
                    ships[xy].sunk = True
            elif len(movement_cmd) == 2:
                # move
                navigation_cmd = movement_cmd[1]
                if ships.has_key(xy) and not ships[xy].sunk:
                    print('Move ship at: {}'.format(xy))
                    print('With navigation: {}'.format(navigation_cmd))
                    new_xy,  _, _, _, _ = ships[xy].move(ships, navigation_cmd, N)
                elif not ships.has_key(xy):
                    print('There is no ship in cell [{}, {}]'.format(x, y))
                elif ships[xy].sunk:
                    print('You cannot move a sunk ship')

    # Output state of board in output.txt
    fprint('Writing final state of board to <{}>'.format(output_txt))
    write_output(output_txt, ships)

if __name__ == '__main__':
    input_txt = 'input.txt'
    output_txt = 'output.txt'
    if len(sys.argv) == 3:
        input_txt = sys.argv[1]
        output_txt = sys.argv[2]
    play(input_txt, output_txt)
