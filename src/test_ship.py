#!/usr/bin/env python
"""Test Ship class"""

import unittest
from ship import Ship


# =======================
# Test ship class methods
# =======================
class TestShip(unittest.TestCase):
    """Testing for Ship class"""

    def test_move(self):
        ships = {
            '0|0': Ship(0, 0, 'N', 0),
            '9|2': Ship(9, 2, 'E', 1)
        }
        N = 10

        # Simple ship movement
        navigation_cmd = 'RRLLR'
        new_state = ships['0|0'].move(ships, navigation_cmd, N)
        self.assertEqual(new_state, ('0|0', 0, 0, 'E', False))
        navigation_cmd = 'MLM'
        new_state = ships['0|0'].move(ships, navigation_cmd, N)
        self.assertEqual(new_state, ('1|1', 1, 1, 'N', False))
        self.assertEqual(ships['1|1'].x, 1)
        self.assertEqual(ships['9|2'].x, 9)
        self.assertEqual(ships.keys(), ['1|1', '9|2'])

        # Move ship outside the board to position (> N) and (< 0)
        navigation_cmd = 'MMRM'
        new_state = ships['9|2'].move(ships, navigation_cmd, N)
        self.assertEqual(new_state, ('9|1', 9, 1, 'S', False))
        navigation_cmd = 'MMM'
        new_state = ships['9|1'].move(ships, navigation_cmd, N)
        self.assertEqual(new_state, ('9|0', 9, 0, 'S', False))
        self.assertEqual(ships['1|1'].x, 1)
        self.assertEqual(ships['9|0'].x, 9)
        self.assertEqual(ships.keys(), ['1|1', '9|0'])

        # Move ship to an occupied cell
        navigation_cmd = 'RMMMMMMMMRM'
        new_state = ships['9|0'].move(ships, navigation_cmd, N)
        self.assertEqual(new_state, ('9|0', 9, 0, 'S', False))
        self.assertEqual(ships['1|1'].x, 1)
        self.assertEqual(ships['9|0'].x, 9)
        self.assertEqual(ships.keys(), ['1|1', '9|0'])

        # Move ship to an occupied cell with a sunk ship
        navigation_cmd = 'RMMMMMMMMRM'
        ships['1|1'].sunk = True
        new_state = ships['9|0'].move(ships, navigation_cmd, N)
        self.assertEqual(new_state, ('9|0', 9, 0, 'S', False))
        self.assertEqual(ships['1|1'].x, 1)
        self.assertEqual(ships['9|0'].x, 9)
        self.assertEqual(ships.keys(), ['1|1', '9|0'])

    def test_output_state(self):
        ship = Ship(0, 0, 'E', 0)
        self.assertEqual(ship.output_state(), '(0, 0, E)')
        ship = Ship(9, 1, 'W', 2)
        ship.sunk = True
        self.assertEqual(ship.output_state(), '(9, 1, W) SUNK')


if __name__ == '__main__':
    unittest.main()
