#!/usr/bin/env python
"""Test functions in play module"""

import unittest
import os
from play import initialize_ships, ships_on_board, parse_movement, write_output
from ship import Ship, ROOT
from os.path import join as pjoin


# =====================================
# Unit test functions in play module
# =====================================
class TestPlay(unittest.TestCase):
    """Testing for play module"""

    def test_initialize_ships(self):
        # test line format given in input.txt
        line = '(0, 0, N) (9, 2, E)'
        ships = initialize_ships(line)
        ship_0 = [ships['0|0'].x, ships['0|0'].y, ships['0|0'].orientation,ships['0|0'].index]
        ship_1 = [ships['9|2'].x, ships['9|2'].y, ships['9|2'].orientation,ships['9|2'].index]
        self.assertEqual(sorted(ships.keys()), sorted(['0|0', '9|2']))
        self.assertEqual(ship_0, [0, 0, 'N', 0])
        self.assertEqual(ship_1, [9, 2, 'E', 1])
        self.assertNotEqual(ship_1, [9, 2, 'E', 0])

        # define 3 ships correctly with varation in spaces
        line = '  (0,0,N)   (9,2,E)(5, 5,S)     '
        ships = initialize_ships(line)
        ship_0 = [ships['0|0'].x, ships['0|0'].y, ships['0|0'].orientation,ships['0|0'].index]
        ship_1 = [ships['9|2'].x, ships['9|2'].y, ships['9|2'].orientation,ships['9|2'].index]
        ship_2 = [ships['5|5'].x, ships['5|5'].y, ships['5|5'].orientation,ships['5|5'].index]
        self.assertEqual(sorted(ships.keys()), sorted(['0|0', '9|2', '5|5']))
        self.assertEqual(ship_0, [0, 0, 'N', 0])
        self.assertEqual(ship_1, [9, 2, 'E', 1])
        self.assertNotEqual(ship_1, [9, 2, 'E', 0])
        self.assertEqual(ship_2, [5, 5, 'S', 2])

        # define 3 ships - one incorrectly
        line = '  (0,0,N)   (9,2,E)(5, 5,Sa)     '
        message = 'Incorrect line declaration'
        try:
            initialize_ships(line)
            message = 'Should not reach this line'
        except:
            pass
        self.assertEqual(message, 'Incorrect line declaration')
        line = '  (0,0,N)   (9,2,E)(5, 5,"S")     '
        message = 'Incorrect line declaration'
        try:
            initialize_ships(line)
            message = 'Should not reach this line'
        except:
            pass
        self.assertEqual(message, 'Incorrect line declaration')

        # define no ship
        line = ''
        message = 'Incorrect line declaration'
        try:
            initialize_ships(line)
            message = 'Should not reach this line'
        except:
            message = 'no ships'
        self.assertEqual(message, 'no ships')

        # define only one ship - incorrectly
        line = '(5, 5,"S") '
        message = 'Incorrect line declaration'
        try:
            initialize_ships(line)
            message = 'Should not reach this line'
        except:
            pass
        self.assertEqual(message, 'Incorrect line declaration')

    def test_ships_on_board(self):
        N = 5
        line = '  (0,0,N)   (9,2,E)(5, 5,S)     '
        ships = initialize_ships(line)
        ships = ships_on_board(ships, N)
        self.assertEqual(ships.keys(), ['0|0'])

    def test_parse_movement(self):
        # define movement
        line = '(0, 0) MRMLMM'
        res = parse_movement(line)
        self.assertEqual(res, ((0, 0), 'MRMLMM'))

        # define shoot
        line = '(9, 2)'
        res = parse_movement(line)
        self.assertEqual(res, ((9, 2),))

        # define wrong coordinates in movement
        line = '(0, A) MRMLMM'
        message = 'Incorrect ship coordinates'
        try:
            parse_movement(line)
            message = 'Should not reach this line'
        except:
            pass
        self.assertEqual(message, 'Incorrect ship coordinates')

        # define wrong coordinates in shoot
        line = '(0, A)'
        message = 'Incorrect ship coordinates'
        try:
            parse_movement(line)
            message = 'Should not reach this line'
        except:
            pass
        self.assertEqual(message, 'Incorrect ship coordinates')

        # define wrong navigation command in movement
        line = '(0, 1) MRAA'
        message = 'Incorrect ship navigation'
        try:
            parse_movement(line)
            message = 'Should not reach this line'
        except:
            pass
        self.assertEqual(message, 'Incorrect ship navigation')

    def test_write_output(self):
        line = '  (0,0,N)   (9,2,E)(5, 5,S)     '
        ships = initialize_ships(line)
        ships['9|2'].sunk = True
        write_output('output_test.txt', ships)
        output_root = pjoin(ROOT, 'data')

        # validate output file creation
        self.assertTrue('output_test.txt' in os.listdir(output_root))

        # validate correct file content
        output_path = pjoin(ROOT, 'data', 'output_test.txt')
        output_f = open(output_path, 'r')
        lines = []
        for line in output_f:
            lines.append(line)
        self.assertEqual(lines[0].strip(), '(0, 0, N)')
        self.assertEqual(lines[1].strip(), '(9, 2, E) SUNK')
        self.assertEqual(lines[2].strip(), '(5, 5, S)')


if __name__ == '__main__':
    unittest.main()
