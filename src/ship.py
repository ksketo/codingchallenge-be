#!/usr/bin/env python
"""Module for Ship class declaration"""

import sys
import ast
import re
import os
from os.path import join as pjoin


# Helper constants for movement
MOVE_DICT = {
    'N': {'x':0, 'y':1},
    'E': {'x':1, 'y':0},
    'S': {'x':0, 'y':-1},
    'W': {'x':-1, 'y':0}
}

LR_DICT = {
    'L': -1,
    'R': 1
}

ORIENTATION_ARRAY = 'NESW'

# Root directory
ROOT = '/'.join(os.getcwd().split('/')[:-1])


class Ship(object):
    """Class for each ship on the board

    This class helps keeping track of each ships position, orientation,sunk
    state and declares methods that define how the ship instance should move
    on the board.
    """

    def __init__(self, x, y, orientation, index):
        """Ship class __init__ method.

        Args:
            x (int): ship coordinate in the x axis (horizontal) of the board.
            y (int): ship coordinate in the y axis (vertical) of the board.
            orientation (str): orientation of ship in the board ('N', 'E', 'S', 'W')
            index (int): Order of ship position in the second line of input.txt.
                         Used to print the ship position in the output, the same
                         order their position was declared

        """
        self.x = x
        self.y = y
        self.orientation = orientation
        self.index = index

        #: boolean: Sunk status of the ship
        self.sunk = False

    def move(self, ships, navigation_cmd, N):
        """Method for moving ship and changing orientation.

        Args:
            ships (dict): Dictionary with all ships in the board.
            navigation_cmd (str): The second parameter.
            N (int): The size of the board.

        Returns:
            A string with the final position of the ship.

        """
        x = self.x
        y = self.y
        orientation = self.orientation
        old_xy = '{}|{}'.format(x, y)

        for m in navigation_cmd:
            if m == 'M':
                # Move ship
                x += MOVE_DICT[orientation]['x']
                x = N-1 if (x > N-1) else max(x, 0)
                y += MOVE_DICT[orientation]['y']
                y = N-1 if (y > N-1) else max(y, 0)
            else:
                # Change orientation
                or_index = ORIENTATION_ARRAY.index(orientation)
                orientation = ORIENTATION_ARRAY[(or_index + LR_DICT[m]) % 4]

        xy = '{}|{}'.format(x, y)
        # Check if ship's final position is to an occupied cell
        if not ships.has_key(xy) or (self.x == x and self.y == y):
            self.x = x
            self.y = y
            self.orientation = orientation
            ships[xy] = self
        else:
            print('This cell is already occupied')

        # remove old entry
        xy = '{}|{}'.format(self.x, self.y)
        if old_xy != xy:
            ships.pop(old_xy, None)

        return (xy, self.x, self.y, self.orientation, self.sunk)

    def output_state(self):
        """Method for outputting ship's position, orientation & sunk status.

        Returns:
            A string with the final state of the ship.

        """

        output_s = "({}, {}, {})".format(self.x, self.y, self.orientation)
        if self.sunk:
            output_s += " SUNK"

        return output_s
